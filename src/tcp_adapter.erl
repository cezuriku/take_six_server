%% @doc Tcp Adapter
-module(tcp_adapter).

-behaviour(gen_tcp_server).

%% API
-export([run/1, new_player/2, game_ready/3, choose_row/1,
         board_change/2, game_finished/2]).

%% gen_tcp_server callbacks
-export([handle_accept/1,
         handle_tcp/3,
         handle_close/3,
         handle_call/3,
         handle_cast/2]).

-include("player.hrl").

-type state_value() :: waiting_name | waiting_players | waiting_turn |
                       waiting_reveal | waiting_row.

-record(data, {state = waiting_name :: state_value(),
               room_pid             :: pid(),
               player_id            :: integer(),
               socket               :: term()
               }).

%%%-----------------------------------------------------------------------------
%%% API functions
%%%-----------------------------------------------------------------------------

%% @doc Start a TCP adapter server
-spec run(integer()) -> ok.
run(Port) ->
    io:format("Server started on port ~p~n", [Port]),
    gen_tcp_server:start_link(?MODULE, Port).

new_player(Pid, Player) ->
    gen_tcp_server:cast(Pid, {new_player, Player}).

game_ready(Pid, Cards, Board) ->
    gen_tcp_server:cast(Pid, {game_ready, Cards, Board}).

choose_row(Pid) ->
    gen_tcp_server:cast(Pid, choose_row).

board_change(Pid, Actions) ->
    gen_tcp_server:cast(Pid, {board_change, Actions}).

game_finished(Pid, Scores) ->
    gen_tcp_server:cast(Pid, {game_finished, Scores}).

%%%-----------------------------------------------------------------------------
%%% gen_tcp_server_handler callbacks
%%%-----------------------------------------------------------------------------

%% @private
handle_accept(Socket) ->
    {ok, #data{state = waiting_name, socket = Socket}}.

%% @private
handle_tcp(Socket, Packet, #data{state = waiting_name} = Data) ->
    << PacketSize:32/big-unsigned-integer,
       1:32/big-unsigned-integer,
       0:32/big-unsigned-integer,
       Size:32/big-unsigned-integer,
       BinaryName/binary>> = Packet,

    % Code[4] + Size[4] + Name[Size]
    PacketSize = 12 + Size,
    Name = erlang:binary_to_list(BinaryName),
    Size = length(Name),

    {ok, Room} = room_manager:get_room_available(),
    {ok, {Id, Players}} = take_6_room:new_connection(Room, self(), Name),

    {NbPlayers, PlayerPacketSize, BinaryPlayers} = lists:foldl(
        fun(#player{id = IdP, name = NameP}, {N, Pps, L}) ->
            SizeNameP = length(NameP),
            BinaryNameP = list_to_binary(NameP),
            {N + 1,
            Pps + 8 + SizeNameP,
            [<<IdP:32/big-unsigned-integer,
              SizeNameP:32/big-unsigned-integer,
              BinaryNameP/binary>>|L]}
        end,
        {0, 0, <<>>}, Players),
    SendPacketSize = PlayerPacketSize + 12,

    Response = [<<SendPacketSize:32/big-unsigned-integer,
                  1:32/big-unsigned-integer,
                  Id:32/big-unsigned-integer,
                  NbPlayers:32/big-unsigned-integer>> |
                  BinaryPlayers],
    gen_tcp:send(Socket, Response),

    {ok, Data#data{state = waiting_players, room_pid = Room,
                   player_id = Id}};
handle_tcp(_Socket, Packet, #data{state = waiting_players,
                                  room_pid = Room} = Data) ->
    << 4:32/big-unsigned-integer,
       10:32/big-unsigned-integer>> = Packet,
    take_6_room:start_playing(Room),
    {ok, Data};
handle_tcp(_Socket, Packet, #data{state = waiting_turn,
                                  player_id = Id,
                                  room_pid = Room} = Data) ->
    << 8:32/big-unsigned-integer,
       20:32/big-unsigned-integer,
       Card:32/big-unsigned-integer>> = Packet,
    take_6_room:play(Room, Id, Card),
    {ok, Data#data{state = waiting_reveal}};
handle_tcp(_Socket, Packet, #data{state = waiting_row,
                                  player_id = Id,
                                  room_pid = Room} = Data) ->
    << 8:32/big-unsigned-integer,
       32:32/big-unsigned-integer,
       Row:32/big-unsigned-integer>> = Packet,
    take_6_room:choose_row(Room, Id, Row),
    {ok, Data#data{state = waiting_reveal}}.

%% @private
handle_close(_Socket, _Reason, #data{state = waiting_players,
                                     room_pid = Room,
                                     player_id = Id}) ->
    take_6_room:remove_connection(Room, Id),
    ok;
handle_close(_Socket, _Reason, _State) ->
    ok.

handle_call(_Msg, _From, Data) ->
    {reply, ok, Data}.

handle_cast({new_player, Player},
             #data{socket = Socket, state = waiting_players} = Data) ->
    gen_tcp:send(Socket, create_packet({new_player, Player})),
    {noreply, Data};
handle_cast({game_ready, Cards, Board},
             #data{socket = Socket, state = waiting_players} = Data) ->
    io:format("Cards ~p Board ~p~n", [Cards, Board]),
    gen_tcp:send(Socket, create_packet({game_ready, Cards, Board})),
    {noreply, Data#data{state = waiting_turn}};
handle_cast(choose_row,
            #data{socket = Socket, state = waiting_reveal} = Data) ->
    gen_tcp:send(Socket, create_packet({choose_row})),
    {noreply, Data#data{state = waiting_row}};
handle_cast({board_change, Actions},
            #data{socket = Socket, state = waiting_reveal} = Data) ->
    gen_tcp:send(Socket, create_packet({board_change, Actions})),
    {noreply, Data#data{state = waiting_turn}};
handle_cast({game_finished, Scores},
            #data{socket = Socket, state = waiting_turn}) ->
    gen_tcp:send(Socket, create_packet({game_finished, Scores})),
    {stop, game_finished}.

create_packet({game_finished, Scores}) ->
    {Size, ScoresPacket} = create_packet({scores, Scores}),
    create_packet({Size, 40, ScoresPacket});
create_packet({board_change, Actions}) ->
    {Size, ActionsPacket} = create_packet({actions, Actions}),
    create_packet({Size, 30, ActionsPacket});
create_packet({choose_row}) ->
    create_packet({0, 31, []});
create_packet({game_ready, Cards, Board}) ->
    {SizeCards, PacketCards} = create_packet({cards, Cards}),
    {SizeBoard, PacketBoard} = create_packet({new_board, lists:keysort(2, Board)}),
    Size = SizeCards + SizeBoard,
    create_packet({Size, 11, [PacketCards | PacketBoard]});
create_packet({new_player, Player}) ->
    {Size, PlayerPacket} = create_packet({id_name, Player}),
    create_packet({Size, 2, PlayerPacket});
create_packet({id_name, #player{id = IdP, name = NameP}}) ->
    SizeNameP = length(NameP),
    BinaryNameP = list_to_binary(NameP),
    {8 + SizeNameP, <<IdP:32/big-unsigned-integer,
                      SizeNameP:32/big-unsigned-integer,
                      BinaryNameP/binary>>};
create_packet({actions, [{Status, Id, Card, Row, _} | OtherActions]}) ->
    create_packet({actions, [{Status, Id, Card, Row} | OtherActions]});
create_packet({actions, [{Status, Id, Card, Row} | OtherActions]}) ->
    {Size, Packet} = create_packet({actions, OtherActions}),
    {Size + 16, [[get_binary_value(Status) |
        <<Id:32/big-unsigned-integer, Card:32/big-unsigned-integer,
          Row:32/big-unsigned-integer>>] | Packet]};
create_packet({actions, []}) ->
    {0, []};
create_packet({cards, [Card | Cards]}) ->
    {Size, Packet} = create_packet({cards, Cards}),
    {Size + 4, [<<Card:32/big-unsigned-integer>> | Packet]};
create_packet({cards, []}) ->
    {0, []};
create_packet({scores, [{Id, Score}| Scores]}) ->
    {Size, Packet} = create_packet({scores, Scores}),
    {Size + 8, [<<Id:32/big-unsigned-integer,
                  Score:32/big-unsigned-integer>> | Packet]};
create_packet({scores, []}) ->
    {0, []};
create_packet({new_board, [{[Card|_], _} | Cards]}) ->
    {Size, Packet} = create_packet({new_board, Cards}),
    {Size + 4, [<<Card:32/big-unsigned-integer>> | Packet]};
create_packet({new_board, []}) ->
    {0, []};
create_packet({Size, Code, List}) ->
    NewSize = Size + 4,
    [<<NewSize:32/big-unsigned-integer,
       Code:32/big-unsigned-integer>> | List].

get_binary_value(success) ->
    <<0:32/big-unsigned-integer>>;
get_binary_value(row_taken) ->
    <<1:32/big-unsigned-integer>>;
get_binary_value(overflow) ->
    <<2:32/big-unsigned-integer>>.
