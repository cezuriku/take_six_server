-module(take_6_room_supervisor).
-behaviour(supervisor).

-export([start_link/0,new_room/0]).
-export([init/1]).

start_link() ->
    supervisor:start_link({local,?MODULE}, ?MODULE, []).

init(_Args) ->
    SupFlags = #{strategy => simple_one_for_one,
                 intensity => 0,
                 period => 1},
    ChildSpecs = [#{id => take_6_room_id,
                    start => {take_6_room, start_link, []},
                    shutdown => 5000,
                    type => worker}],
    {ok, {SupFlags, ChildSpecs}}.

new_room() ->
    supervisor:start_child(?MODULE, []).
