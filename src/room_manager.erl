-module(room_manager).
-behaviour(gen_server).

-export([start_link/0, get_room_available/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%%% Client API
start_link() ->
    gen_server:start_link({local, room_manager}, ?MODULE, [], []).

%% Retrieve or create a room with at least one place
get_room_available() ->
   gen_server:call(room_manager, room_available).

%%% Server functions
init([]) -> {ok, #{room => undefined}}. %% no treatment of info here!

handle_call(room_available, _From, #{room := Room} = Data) ->
    case Room of
        undefined ->
            {ok, NewRoom} = take_6_room_supervisor:new_room();
        _ ->
            NewRoom = Room
    end,
    {reply, {ok, NewRoom}, Data#{room => NewRoom}}.

handle_cast(Msg, State) ->
    io:format("Receive message ~p~n", [Msg]),
    {noreply, State}.

handle_info(Msg, State) ->
    io:format("Unexpected message: ~p~n",[Msg]),
    {noreply, State}.

terminate(Reason, _State) ->
    io:format("Stop: ~p~n",[Reason]),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Private functions

