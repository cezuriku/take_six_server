%%%-------------------------------------------------------------------
%% @doc take_six_server top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(take_six_server_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    {ok, { {one_for_all, 1, 5}, [
    #{id => tcp_adapter,
      start => {tcp_adapter, run, [5678]},
      restart => permanent,
      shutdown => 5000,
      type => worker},
    #{id => take_six_room_manager,
      start => {room_manager, start_link, []},
      restart => permanent,
      shutdown => 5000,
      type => worker},
    #{id => take_six_room_supervisor,
      start => {take_6_room_supervisor, start_link, []},
      restart => permanent,
      shutdown => 10000,
      type => supervisor}
    ]} }.

%%====================================================================
%% Internal functions
%%====================================================================
