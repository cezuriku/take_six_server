-module(take_6_room).
-behaviour(gen_statem).

-export([start_link/0,stop/0]).
-export([new_connection/3,remove_connection/2,start_playing/1,play/3,
         choose_row/3]).
-export([init/1,callback_mode/0,terminate/3]).
% States
-export([waiting/3,in_game/3,reveal_game/3,reveal_scores/3]).

%~ -type state() :: atom().
-include("player.hrl").

-record(game, {cards = []           :: [integer],
               board = []           :: [{[integer()], integer()}],
               turns = 10           :: integer(),
               cards_played = []    :: [{integer(), integer()}]
               }).

-record(data, {players = []         :: [ #player{} ],
               ids_available = []   :: integer(),
               game = #game{}       :: #game{},
               nb_players           :: integer()
               }).

start_link() ->
    gen_statem:start_link(?MODULE, [], []).

stop() ->
    gen_statem:stop(self()).

new_connection(RoomPid, Pid, Name) ->
    gen_statem:call(RoomPid, {new_connection, {Pid, Name}}).

remove_connection(RoomPid, Id) ->
    gen_statem:cast(RoomPid, {remove_connection, Id}).

start_playing(RoomPid) ->
    gen_statem:call(RoomPid, {start_playing}).

play(RoomPid, Id, Card) ->
    gen_statem:cast(RoomPid, {play, Id, Card}).

choose_row(RoomPid, Id, Row) ->
    gen_statem:cast(RoomPid, {reveal, Id, Row}).

init([]) ->
    {ok, waiting, #data{}}.

callback_mode() ->
    state_functions.

terminate(_Reason, _State, _Data) ->
    ok.

waiting({call, From}, {new_connection, {Pid, Name}},
        #data{players = Players, ids_available = IdsAvailable} = Data) ->
    % Look if an id is available if a player leaves
    case IdsAvailable of
        [] ->
            NewIdsAvailable = [],
            NewId = length(Players);
        [LastId | NewIdsAvailable] ->
            NewId = LastId
    end,

    NewPlayer = #player{ id = NewId, pid = Pid, name = Name},
    io:format("New player [~p]~p~n", [NewId, Name]),
    inform_new_player(Players, NewPlayer),

    {keep_state, Data#data{players = [NewPlayer | Players],
                           ids_available = NewIdsAvailable},
     [{reply, From, {ok, {NewId, Players}}}]};
waiting(cast, {remove_connection, Id},
        #data{players = Players, ids_available = IdsAvailable} = Data) ->
    NewPlayers = lists:filtermap(fun(#player{id = PlayerId,
                                             name = PlayerName} = Player) ->
        case PlayerId of
            Id ->
                io:format("Lost player [~p]~p~n", [Id, PlayerName]),
                false;
            _ -> {true, Player}
        end
    end, Players),

    {keep_state, Data#data{players = NewPlayers,
                           ids_available = [Id | IdsAvailable]}};
waiting({call, From}, {start_playing},
        #data{players = Players, game = Game} = Data) ->
    Deck = create_deck(),
    {NewPlayers, Board, NewDeck} = distribute_cards(Players, Deck),
    io:format("Board ~p~n", [Board]),

    inform_game_ready(NewPlayers, Board),

    {next_state, in_game, Data#data{players = NewPlayers,
                                    nb_players = length(NewPlayers),
                                    game = Game#game{cards = NewDeck,
                                                     board = Board}},
        [{reply, From, ok}]}.

in_game(cast, {play, Id, Card},
        #data{game = #game{cards_played = CardsPlayed,
                           turns = Turns} = Game, players = Players,
              nb_players = NbPlayers} = Data) ->
    {{true, Player}, OtherPlayers} = lists:foldl(
        fun(#player{id = PlayerId} = P, {Find, ListPlayers}) ->
            case PlayerId of
                Id ->
                    {{true, P}, ListPlayers};
                _ ->
                    {Find, [P | ListPlayers]}
            end
        end,
        {false, []}, Players),
    {true, NewCards} = lists:foldl(
        fun(C, {Find, ListCard}) ->
            case C of
                Card ->
                    {true, ListCard};
                _ ->
                    {Find, [C | ListCard]}
            end
        end,
        {false, []}, Player#player.cards),
    NewPlayer = Player#player{cards = NewCards},
    NewPlayers = [NewPlayer | OtherPlayers],
    io:format("[~p] plays ~p ~p~n", [Id, Card, NewPlayer]),
    case length(CardsPlayed) + 1 of
        NbPlayers ->
            {next_state, reveal_game, Data#data{players = NewPlayers,
                game = Game#game{cards_played = lists:sort([{Card, Id} | CardsPlayed]),
                                 turns = Turns - 1}},
                {next_event, cast, reveal}};
        _ ->
            {keep_state, Data#data{players = NewPlayers,
                game = Game#game{cards_played = [{Card, Id} | CardsPlayed]}}}
    end.

reveal_game(cast, {reveal, Id, Row},
        #data{game = #game{cards_played = CardsPlayed, turns = Turns,
                           board = Board} = Game, players = Players} = Data) ->
    io:format("Player~p choose the row ~p~n", [Id + 1, Row]),
    {ok, NewBoard, Actions} = play_cards({choose_row, Row}, CardsPlayed, Board),
    NewPlayers = compute_actions(Players, Actions),
    io:format("Actions ~p~n", [Actions]),
    io:format("Board ~p~n", [NewBoard]),
    inform_board_change(NewPlayers, lists:reverse(Actions)),
    case Turns of
        0 ->
            {next_state, reveal_scores,
                Data#data{players = NewPlayers,
                          game = Game#game{board = NewBoard}},
                     {next_event, cast, reveal}};
        _ ->
            {next_state, in_game,
                Data#data{players = NewPlayers,
                          game = Game#game{cards_played = [],
                                           board = NewBoard}}}
    end;
reveal_game(cast, reveal,
        #data{players = Players,
              game = #game{cards_played = [{_,PlayerId}|_] = CardsPlayed,
                           board = Board, turns = Turns} = Game} = Data) ->
    {Status, NewBoard, Actions} = play_cards(CardsPlayed, Board, []),
    NewPlayers = compute_actions(Players, Actions),
    io:format("Actions ~p~n", [Actions]),
    io:format("Board ~p~n", [NewBoard]),
    case Status of
        error ->
            {keep_state_and_data, {next_event, cast, {choose_row, PlayerId}}};
        _ ->
            inform_board_change(NewPlayers, lists:reverse(Actions)),
            case Turns of
                0 ->
                    {next_state, reveal_scores,
                        Data#data{players = NewPlayers,
                                  game = Game#game{board = NewBoard}},
                     {next_event, cast, reveal}};
                _ ->
                    {next_state, in_game,
                        Data#data{players = NewPlayers,
                                  game = Game#game{cards_played = [],
                                  board = NewBoard}}}
            end
    end;
reveal_game(cast, {choose_row, Id},
        #data{players = Players}) ->
    Player = lists:keyfind(Id, #player.id, Players),

    io:format("Player~p needs to choose a row~n", [Id + 1]),
    inform_choose_row(Player),
    keep_state_and_data.
reveal_scores(cast, reveal, #data{players = Players}) ->
    Scores = compute_scores(Players),
    io:format("Scores ~p~n", [Scores]),
    inform_scores(Players, Scores),
    {stop, the_end}.

% private
-spec inform_new_player([#player{}], #player{}) -> ok.
inform_new_player([#player{pid = Pid}|Players], NewPlayer) ->
    ok = tcp_adapter:new_player(Pid, NewPlayer),
    inform_new_player(Players, NewPlayer);
inform_new_player([], _) ->
    ok.

-spec inform_board_change([#player{}], []) -> ok.
inform_board_change([#player{pid = Pid}|Players], Actions) ->
    ok = tcp_adapter:board_change(Pid, Actions),
    inform_board_change(Players, Actions);
inform_board_change([], _) ->
    ok.

-spec inform_game_ready([#player{}], []) -> ok.
inform_game_ready([#player{pid = Pid,
                          cards = Cards}|Players], Board) ->
    ok = tcp_adapter:game_ready(Pid, Cards, Board),
    inform_game_ready(Players, Board);
inform_game_ready([], _) ->
    ok.

inform_scores([#player{pid = Pid} | Players], Scores) ->
    ok = tcp_adapter:game_finished(Pid, Scores),
    inform_scores(Players, Scores);
inform_scores([], []) ->
    ok.

inform_choose_row(#player{pid = Pid}) ->
    ok = tcp_adapter:choose_row(Pid),
    ok.

create_deck() ->
    [X||{_,X} <- lists:sort([ {rand:uniform(), N} ||
                                N <- lists:seq(1,104)])].

distribute_cards(Players, Deck) ->
    distribute_cards([], Players, Deck).

distribute_cards(NewPlayers, [Player | Players], Deck) ->
    {Hand, NewDeck} = lists:split(10, Deck),
    NewPlayer = Player#player{cards = Hand},
    distribute_cards([NewPlayer | NewPlayers], Players, NewDeck);
distribute_cards(NewPlayers, [], Deck) ->
    {BoardCards, NewDeck} = lists:split(4, Deck),
    Board = lists:zip([[C] || C <- BoardCards], lists:seq(0, 3)),
    {NewPlayers, [C || C <- lists:sort(Board)], NewDeck}.

% The cards and the board need to be sorted
play_cards({choose_row, RowId}, [{PlayerCard, PlayerId}|OtherCards], Board) ->
    {Row, _} = lists:keyfind(RowId, 2, Board),
    FinalBoard = lists:keyreplace(RowId, 2, Board, {[PlayerCard], RowId}),
    Action = {row_taken, PlayerId, PlayerCard, RowId, Row},
    play_cards(OtherCards, lists:sort(FinalBoard), [Action]);
play_cards([{PlayerCard, PlayerId}|OtherCards] = Cards,
           [{[FirstCardBoard|_] = Row,RowId}|OtherBoard] = Board,
           ActionList) ->
    case PlayerCard < FirstCardBoard of
        true ->
            {error, Board, ActionList};
        false ->
            {Status, NewBoard, NewActionList} = play_cards(Cards, OtherBoard, ActionList),
            case Status of
                error ->
                    case length(Row) of
                        5 ->
                            FinalBoard = [{[PlayerCard],RowId}|OtherBoard],
                            Action = {overflow, PlayerId, PlayerCard, RowId, Row};
                        _ ->
                            FinalBoard = [{[PlayerCard|Row],RowId}|OtherBoard],
                            Action = {success, PlayerId, PlayerCard, RowId}
                    end,
                    play_cards(OtherCards, FinalBoard, [Action | ActionList]);
                ok ->
                    {Status, [{Row, RowId} | NewBoard], NewActionList}
            end
    end;
play_cards(_, [], ActionList) ->
    {error, [], ActionList};
play_cards([], Board, ActionList) ->
    {ok, Board, ActionList}.

compute_actions(Players, [{_, PlayerId, _, _, Cards} | OtherActions]) ->
    Player = lists:keyfind(PlayerId, #player.id, Players),
    NewPlayers = lists:keyreplace(PlayerId, #player.id, Players,
            Player#player{ cards_taken = lists:append(Player#player.cards_taken, Cards)}),
    compute_actions(NewPlayers, OtherActions);
compute_actions(Players, [{success, _, _, _} | OtherActions]) ->
    compute_actions(Players, OtherActions);
compute_actions(Players, []) ->
    Players.

compute_scores(Players) ->
    lists:map(
        fun(#player{id = Id, cards_taken = Cards}) ->
            {Id, lists:foldl(fun(X, Score) ->
                                compute_value(X) + Score
                             end, 0, Cards)}
        end, Players).

compute_value(55) ->
    7;
compute_value(X) when X rem 11 =:= 0 ->
    5;
compute_value(X) when X rem 10 =:= 0 ->
    3;
compute_value(X) when X rem 5 =:= 0 ->
    2;
compute_value(_) ->
    1.
