pipeline {
  agent any

  environment {
    registry = "docker.fingeros.me/"
    registryCredential = '0a5601b8-04d6-492f-b2a8-09494749d854'
  }

  stages {
    stage('Mattermost notification') {
      steps {
        mattermostSend color: 'good', endpoint: 'https://mattermost.gnous.ovh/hooks/x16as7tk97ra5bfr9x4cm7pi1c', message: "started ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)", text: 'second text'
      }
    }

    stage('Compile and test') {
      parallel {
        stage('amd64') {
          agent {
            docker {
              label 'amd64'
              image 'erlang:21'
            }
          }

          stages {
            stage('Checkout') {
              steps {
                checkout scm
              }
            }

            stage('Build') {
              steps {
                sh 'rebar3 compile'
              }
            }

            stage('Test') {
              steps {
                sh 'rebar3 eunit'
              }
            }

            stage('Build release') {
              steps {
                sh 'rebar3 release'
              }
            }
          }
        }

        stage('arm') {
          agent {
            docker {
              label 'arm'
              image 'erlang:21'
            }
          }

          stages {
            stage('Checkout') {
              steps {
                checkout scm
              }
            }

            stage('Build') {
              steps {
                sh 'rebar3 compile'
              }
            }

            stage('Test') {
              steps {
                sh 'rebar3 eunit'
              }
            }

            stage('Build release') {
              steps {
                sh 'rebar3 release'
              }
            }
          }
        }
      }
    }

    stage('Build & push docker images') {
      parallel {
        stage('amd64') {
          agent {
            node {
              label 'amd64'
            }
          }

          steps {
            script {
              withDockerRegistry(credentialsId: registryCredential,
                url: "https://" + registry) {
                def image = docker.build("docker.fingeros.me/take-six-server-amd64")

                image.push()
              }
            }
          }
        }

        stage('arm') {
          agent {
            node {
              label 'arm'
            }
          }

          steps {
            script {
              withDockerRegistry(credentialsId: registryCredential,
                url: "https://" + registry) {
                def image = docker.build(registry + "take-six-server-arm")

                image.push()
              }
            }
          }
        }
      }
    }
  }
}
