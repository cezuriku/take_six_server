take_six_server
=====

A server for the card game Take 6! written in erlang

Build
-----

    $ rebar3 compile

Try
-----

    $ rebar3 shell

Test
-----

    $ rebar3 eunit

Make release
-----

    $ rebar3 release
