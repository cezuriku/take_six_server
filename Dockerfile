FROM erlang:21

WORKDIR /home
COPY _build/default/rel/alligator alligator

EXPOSE 5678
CMD ["/home/alligator/bin/alligator", "start"]
